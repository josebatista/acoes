package br.com.esi.usp.acoes;

import android.app.ActionBar;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.esi.usp.acoes.adapter.ListaAcoesAdapter;
import br.com.esi.usp.acoes.model.Acao;

public class ListaAcoesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_acoes);
        carregaLista();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater =getMenuInflater();
        menuInflater.inflate(R.menu.menu_acoes,menu);
        return  true;
    }

    private void incluirAtivo(){
       Toast toast = Toast.makeText(this,"TESTANDO", Toast.LENGTH_LONG);
       toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.menu_acoes_incluir)
            incluirAtivo();

        return true;
    }

    private void carregaLista(){

        List<Acao> acoes = new ArrayList<Acao>();

        acoes.add(new Acao("LAME4","LOJAS AMERICANAS PNE", 16, 0.41, 15.53));
        acoes.add(new Acao("BPAC","BANCO PACTUAL", 16.65, 2.41, 14.31));

        ListView lista = (ListView)findViewById(R.id.lista_acoes);
        ListaAcoesAdapter adapter = new ListaAcoesAdapter(acoes, this);

        lista.setAdapter(adapter);

    }

}
