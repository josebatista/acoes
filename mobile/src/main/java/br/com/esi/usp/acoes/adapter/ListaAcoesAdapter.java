package br.com.esi.usp.acoes.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.esi.usp.acoes.R;
import br.com.esi.usp.acoes.model.Acao;

/**
 * Created by lucas on 17/09/2017.
 */

public class ListaAcoesAdapter extends BaseAdapter {

    private final List<Acao> acoes;
    private final Activity act;

    public ListaAcoesAdapter(List<Acao> acoes, Activity act) {
        this.acoes = acoes;
        this.act = act;
    }

    @Override
    public int getCount() {
        return acoes.size();
    }

    @Override
    public Object getItem(int position) {
        return acoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater()
                .inflate(R.layout.item_lista_acoes, parent, false);
        Acao acao = (Acao)getItem(position);

        //Pegando referencia dos elementos da lista
        TextView  sigla = (TextView) view.findViewById(R.id.acoes_sigla);
        TextView  descricao = (TextView) view.findViewById(R.id.acoes_descricao);
        TextView  valorAtual = (TextView) view.findViewById(R.id.acoes_valor_atual);
        TextView  valorCompra = (TextView) view.findViewById(R.id.acoes_valor_compra);
        TextView  percAtual = (TextView) view.findViewById(R.id.acoes_percentual_atual);
        TextView  percCompra = (TextView) view.findViewById(R.id.acoes_percentual_compra);

        //Setando valores
        sigla.setText(acao.getSigla());
        descricao.setText(acao.getDescricao());
        valorAtual.setText("" + acao.getValorAtual());
        percAtual.setText(acao.getPercAtual() + "%");
        valorCompra.setText(acao.getValorBase() + "");

        return view;
    }
}
